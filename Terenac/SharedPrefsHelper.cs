using Android.Content;
using Android.Preferences;
using System;

namespace Terenac {
	public static class SharedPrefsHelper {
		public static ISharedPreferences GetPrefs(this Context self) {
			return PreferenceManager.GetDefaultSharedPreferences(self);
		}

		public static void SetPrefs(this Context self, Action<ISharedPreferencesEditor> act) {
			var prefs = PreferenceManager.GetDefaultSharedPreferences(self).Edit();
			act.Invoke(prefs);
			prefs.Commit();
		}
	}
}