﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Terenac
{
	public class MenuFragment : Fragment, ISharedPreferencesOnSharedPreferenceChangeListener
	{
		class MenuButton : Button {
			public MenuButton(Context c) : base(c) {}

			public string Uid { get; set; }
		}

		private Button CoreImageTabButton, BasicInfoTabButton, DigInfoTabButton;
		private LinearLayout TerrainListview;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			this.Activity.GetPrefs ().RegisterOnSharedPreferenceChangeListener (this);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var inflated = inflater.Inflate(Resource.Layout.Menu, container, false);

			this.CoreImageTabButton = inflated.FindViewById<Button> (Resource.Id.menuUploadImageTabButton);
			this.BasicInfoTabButton = inflated.FindViewById<Button> (Resource.Id.menuBasicInfoTabButton);
			this.DigInfoTabButton = inflated.FindViewById<Button> (Resource.Id.menuDigsTabButton);
			this.TerrainListview = inflated.FindViewById<LinearLayout> (Resource.Id.menuOldTerrainList);

			var prefs = this.Activity.GetPrefs ();
			CheckCurrentWorkingTerrain (prefs);
			UpdateTerrainList (prefs);
			return inflated;
		}

		public void OnSharedPreferenceChanged (ISharedPreferences sharedPreferences, string key)
		{
			switch (key) {
			case "terenac_current_working_terrain":
				CheckCurrentWorkingTerrain (sharedPreferences);
				break;
			case "terenac_terrain_list":
				UpdateTerrainList (sharedPreferences);
				break;
			default:				
				if (key.EndsWith ("_drill")) {
					UpdateTerrainList (sharedPreferences);
				}
				break;
			}
		}

		private void CheckCurrentWorkingTerrain(ISharedPreferences sharedPreferences) {
			string currentWorkingTerrain = sharedPreferences.GetString ("terenac_current_working_terrain", null);
							
			bool currentWorkingTerrainExists = (currentWorkingTerrain != null);

			this.CoreImageTabButton.Enabled = currentWorkingTerrainExists;
			this.BasicInfoTabButton.Enabled = currentWorkingTerrainExists;
			this.DigInfoTabButton.Enabled = currentWorkingTerrainExists;

			if(currentWorkingTerrainExists)
				this.BasicInfoTabButton.CallOnClick ();
		}

		private void UpdateTerrainList(ISharedPreferences sharedPreferences) {
			this.TerrainListview.RemoveAllViews ();

			string terrainList = sharedPreferences.GetString ("terenac_terrain_list", "");

			var splitTerrainList = terrainList.Split (new char[] { ',' });

			foreach (var terrain in splitTerrainList) {
				if (terrain == "")
					continue;

				var button = new MenuButton (this.Activity);
				button.Uid = terrain;
				button.Text = sharedPreferences.GetString (terrain + "_drill", this.Activity.Resources.GetString(Resource.String.unknown));
				if (button.Text == "")
					button.Text = this.Activity.Resources.GetString (Resource.String.unknown);
				this.TerrainListview.AddView (button);

				button.Click += (sender, e) => {
					this.Activity.SetPrefs ((ISharedPreferencesEditor editor) => editor.PutString ("terenac_current_working_terrain", button.Uid));
					this.BasicInfoTabButton.CallOnClick ();
				};
			}
		}
	}
}

