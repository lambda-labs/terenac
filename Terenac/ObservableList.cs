﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace Terenac
{
	public class ObservableList<T> : List<T>
	{
		private List<T> list;
		public event ListChangedEventDelegate ListChanged;
		public delegate void ListChangedEventDelegate();

		public ObservableList() {
			list = new List<T> ();
		}

		public new void Add(T item)
		{
			list.Add(item);
		}

		public new void Remove(T item) {			
			list.Remove (item);
		}

		public void Notify() {
			if (ListChanged != null)
			{
				ListChanged();
			}
		}

		public List<T> Items {
			get { return list; } 
			set {
				list = value; 
			}
		}
	}			
}

