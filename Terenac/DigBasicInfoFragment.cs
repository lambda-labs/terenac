
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Provider;
using Android.Content.PM;
using Android.Graphics;

namespace Terenac
{
	public class DigBasicInfoFragment : Fragment, ISharedPreferencesOnSharedPreferenceChangeListener
	{
		private Dictionary<string, GridLayout> gridParents = new Dictionary<string, GridLayout>();
		private HashSet<View> followingFocus = new HashSet<View>();
		private GridLayout leftGrid, rightGrid;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			RefreshManually ();
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var inflated = inflater.Inflate(Resource.Layout.DigBasicInfo, container, false);

			leftGrid = inflated.FindViewById<GridLayout> (Resource.Id.gridLayout1);
			rightGrid = inflated.FindViewById<GridLayout> (Resource.Id.gridLayout2);

			KeepValuesInGridUpToDate (leftGrid);
			KeepValuesInGridUpToDate (rightGrid);

			Button deleteTerrain = inflated.FindViewById<Button> (Resource.Id.deleteTerrain);

			deleteTerrain.Click += (object sender, EventArgs e) => {
				Android.App.AlertDialog.Builder builder = new AlertDialog.Builder(this.Activity);
				AlertDialog dialog = builder.Create();
				dialog.SetTitle(this.Activity.Resources.GetString(Resource.String.warning));
				dialog.SetIcon(Resource.Drawable.Icon);
				dialog.SetMessage(this.Activity.Resources.GetString(Resource.String.delete_terrain_warning));

				dialog.SetButton(this.Activity.Resources.GetString(Resource.String.yes), (object tsender, DialogClickEventArgs ev) => {
					var prefs = this.Activity.GetPrefs ();
					var editor = prefs.Edit();

					string terrain = prefs.GetString ("terenac_current_working_terrain", null);

					if(terrain != null) {
						var terrainList = prefs.GetString("terenac_terrain_list", "");
						var splitTerrainList = terrainList.Split (new char[] { ',' });
						var listOfTerrains = new List<string> (splitTerrainList);

						listOfTerrains.Remove(terrain);

						var allEntries = new List<string>(prefs.All.Keys);
						foreach(string entry in allEntries) {
							if(entry.StartsWith(terrain + "_")) {
								editor.Remove(entry);
							}
						}

						editor.PutString ("terenac_terrain_list", String.Join(",", listOfTerrains));

						if(listOfTerrains.Count > 0) {
							string se = listOfTerrains[0];
							if(se == "") se = null;
							editor.PutString("terenac_current_working_terrain", se);
						} else {
							editor.PutString("terenac_current_working_terrain", null);
						}

						editor.Commit ();
					}

					RefreshManually ();					
				});

				dialog.SetButton2(this.Activity.Resources.GetString(Resource.String.no), (s, ev) => RefreshManually ());

				dialog.Show();
			};

			return inflated;
		}

		private void KeepValuesInGridUpToDate(GridLayout grid) {			
			int count = grid.ChildCount;

			string terrain = this.Activity.GetPrefs ().GetString ("terenac_current_working_terrain", null);

			for (int i = 0; i < count; i++) {
				View v = grid.GetChildAt (i);
				if(v is EditText) {
					var ev = ((EditText) v);

					gridParents [ev.Tag.ToString ()] = grid;

					ev.Text = this.Activity.GetPrefs ().GetString (terrain + "_" + ev.Tag.ToString (), "");

					if(!followingFocus.Contains(ev)) {
						ev.FocusChange += (sender, e) => {
							var _terrain = this.Activity.GetPrefs ().GetString ("terenac_current_working_terrain", null);

							if (_terrain == null) {
								Toast.MakeText (this.Activity, this.Activity.Resources.GetString (Resource.String.error_no_terrain_selected), ToastLength.Short).Show ();
							} else {
								this.Activity.SetPrefs ((ISharedPreferencesEditor editor) => editor.PutString (_terrain + "_" + ev.Tag.ToString (), ev.Text));
							}
						};

						followingFocus.Add (ev);
					}
				}
			}
		}

		private void Restrict(GridLayout grid, bool should) {			
			if (grid == null)
				return;
			
			int count = grid.ChildCount;

			for (int i = 0; i < count; i++) {
				View v = grid.GetChildAt (i);

				var editText = v as EditText;
				if(editText != null) editText.Enabled = should;
			}
		}

		public void OnSharedPreferenceChanged (ISharedPreferences sharedPreferences, string key)
		{
			var terrain = sharedPreferences.GetString ("terenac_current_working_terrain", null);

			if (key != "terenac_current_working_terrain") {				
				if (terrain != null) {
					if (key.StartsWith (terrain + "_")) {
						var tag = key.Replace (terrain + "_", "");
						if (gridParents.ContainsKey (tag)) {
							var ctrl = gridParents [tag].FindViewWithTag (tag);
							if (ctrl != null) {
								var ve = ((EditText)ctrl);
								ve.Text = sharedPreferences.GetString (key, ve.Text);
							}
						}
					}
				}
			} else {			
				RefreshManually ();
			}
		}

		public void RefreshManually() {
			if (this.Activity == null)
				return;
			
			var terrain = this.Activity.GetPrefs ().GetString ("terenac_current_working_terrain", null);

			Restrict(leftGrid, terrain != null);
			Restrict(rightGrid, terrain != null);

			if (leftGrid == null || rightGrid == null)
				return;
			
			KeepValuesInGridUpToDate (leftGrid);
			KeepValuesInGridUpToDate (rightGrid);
		}
	}
}
