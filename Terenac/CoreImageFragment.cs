
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Provider;
using Android.Content.PM;
using Android.Graphics;
using Android.Content.Res;

namespace Terenac
{
	public static class BitmapHelpers
	{		
		public static async Task<BitmapFactory.Options> GetBitmapOptionsOfImageAsync(string file)
		{
			BitmapFactory.Options options = new BitmapFactory.Options
			{
				InJustDecodeBounds = true
			};

			Bitmap result = await BitmapFactory.DecodeFileAsync (file);
			return options;
		}

		public static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
		{			
			float height = options.OutHeight;
			float width = options.OutWidth;
			double inSampleSize = 1D;

			if (height > reqHeight || width > reqWidth)
			{
				int halfHeight = (int)(height / 2);
				int halfWidth = (int)(width / 2);

				while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
				{
					inSampleSize *= 2;
				}
			}

			return (int)inSampleSize;
		}

		public static async Task<Bitmap> LoadScaledDownBitmapForDisplayAsync(String file, Resources res, BitmapFactory.Options options, int reqWidth, int reqHeight)
		{
			options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);
			options.InJustDecodeBounds = false;

			return await BitmapFactory.DecodeFileAsync(file, options);
		}
	}

	public class CoreImageFragment : Fragment
	{
		public static class CurrentImageData {
			public static Java.IO.File File;
			public static Bitmap Bitmap;
		}

		private ImageView newCoreImageView;
		private Button newCoreImageButton;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			ReloadImage ();
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var inflated = inflater.Inflate(Resource.Layout.CoreImage, container, false);
			this.newCoreImageView = inflated.FindViewById<ImageView>(Resource.Id.newCoreImageView);
			this.newCoreImageButton = inflated.FindViewById<Button>(Resource.Id.newCoreImageButton);

			if(this.IsCameraAppAvailable()) 
			{
				this.CreateDirectoryForPictures();
				if (CurrentImageData.Bitmap != null) {
					this.newCoreImageView.SetImageBitmap (CurrentImageData.Bitmap);
				}

				this.newCoreImageButton.Click += this.TakeAPicture;
			}

			return inflated;
		}

		private bool IsCameraAppAvailable() 
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			IList<ResolveInfo> availableActivities = this.Activity.PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
		}

		private void CreateDirectoryForPictures()
        {
        	var dir = new Java.IO.File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures), "Terenac");
            if (!dir.Exists())
            {
                dir.Mkdirs();
            }

            this.Activity.SetPrefs((ISharedPreferencesEditor e) => e.PutString ("terenac_images_dir", dir.Path));
        }

        private void TakeAPicture(object sender, EventArgs eventArgs)
        {
        	var dir = this.Activity.GetPrefs().GetString("terenac_images_dir", null);

            CurrentImageData.File = new Java.IO.File(dir, String.Format("core_{0}.jpg", Guid.NewGuid()));

            Intent intent = new Intent(MediaStore.ActionImageCapture);
			intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(CurrentImageData.File));

            StartActivityForResult(intent, 0);
        }

		private async void ReloadImage(string terrainId = null) {			
			var prefs = this.Activity.GetPrefs ();

			if(terrainId == null) {
				terrainId = prefs.GetString("terenac_current_working_terrain", "");			
			}

			string terrainImg = null;

			if (terrainId == null) {
				 terrainImg = prefs.GetString (terrainId + "_image", null);
			}
				
			if (terrainImg != null) {
				int height = Resources.DisplayMetrics.HeightPixels;
				int width = newCoreImageView.Width;

				BitmapFactory.Options options = await BitmapHelpers.GetBitmapOptionsOfImageAsync(terrainImg);
				Bitmap bitmapToDisplay = await BitmapHelpers.LoadScaledDownBitmapForDisplayAsync (terrainImg, Resources, options, width, height);
				newCoreImageView.SetImageBitmap(bitmapToDisplay);
			}				
		}

        public override void OnActivityResult(int requestCode, Result resultCode, Intent data) {
        	base.OnActivityResult(requestCode, resultCode, data);

        	Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
			var contentUri = Android.Net.Uri.FromFile(CurrentImageData.File);
			mediaScanIntent.SetData(contentUri);
			this.Activity.SendBroadcast(mediaScanIntent);

			var terrain = this.Activity.GetPrefs ().GetString ("terenac_current_working_terrain", null);
			if (terrain != null) {
				this.Activity.SetPrefs((ISharedPreferencesEditor editor) => editor.PutString(terrain + "_image", CurrentImageData.File.Path));
			}

			ReloadImage (terrain);
        }
	}
}
