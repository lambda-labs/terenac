﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.App;

namespace Terenac
{
	[Activity (Label = "Terenac", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : FragmentActivity
	{
		private const string FRAG_TAG = "FRAG_TAG";
		private CoreImageFragment coreImageFragment;
		private DigBasicInfoFragment digBasicInfoFragment;
		private LitologicalUnitsFragment litoUnitsFragment;

		protected override void OnCreate (Bundle bundle)
		{	
			base.OnCreate (bundle);
			//this.SetPrefs ((ISharedPreferencesEditor editor) => editor.Clear ());

			SetContentView (Resource.Layout.Main);

			coreImageFragment = new CoreImageFragment ();
			digBasicInfoFragment = new DigBasicInfoFragment ();
			litoUnitsFragment = new LitologicalUnitsFragment ();

			var imageButton = this.FindViewById<Button> (Resource.Id.menuUploadImageTabButton);
			imageButton.Click += (sender, e) => {				
				Android.App.FragmentTransaction transaction = this.FragmentManager.BeginTransaction();

				transaction.SetCustomAnimations(Android.Resource.Animator.FadeIn, Android.Resource.Animator.FadeOut);
				transaction.Replace(Resource.Id.viewFragment, coreImageFragment, FRAG_TAG);
				transaction.AddToBackStack(null);

				transaction.Commit();
			};

			var basicInfoButton = this.FindViewById<Button> (Resource.Id.menuBasicInfoTabButton);
			basicInfoButton.Click += (sender, e) => {
				Android.App.FragmentTransaction transaction = this.FragmentManager.BeginTransaction();

				transaction.SetCustomAnimations(Android.Resource.Animator.FadeIn, Android.Resource.Animator.FadeOut);
				transaction.Replace(Resource.Id.viewFragment, digBasicInfoFragment, FRAG_TAG);
				transaction.AddToBackStack(null);

				transaction.Commit();

				digBasicInfoFragment.RefreshManually();
			};

			var digButton = this.FindViewById<Button> (Resource.Id.menuDigsTabButton);
			digButton.Click += (sender, e) => {
				Android.App.FragmentTransaction transaction = this.FragmentManager.BeginTransaction();

				transaction.SetCustomAnimations(Android.Resource.Animator.FadeIn, Android.Resource.Animator.FadeOut);
				transaction.Replace(Resource.Id.viewFragment, litoUnitsFragment, FRAG_TAG);
				transaction.AddToBackStack(null);

				transaction.Commit();

				litoUnitsFragment.RefreshManually();
			};

			var newTerrainButton = this.FindViewById<Button> (Resource.Id.menuNewTerrainButton);
			newTerrainButton.Click += (sender, e) => CreateNewTerrain();
		}

		private void CreateNewTerrain() {
			var prefs = this.GetPrefs ();
			var editor = prefs.Edit ();
			string guid = System.Guid.NewGuid ().ToString ();

			editor.PutString ("terenac_current_working_terrain", guid);

			string terrainList = prefs.GetString ("terenac_terrain_list", "");

			var splitTerrainList = terrainList.Split (new char[] { ',' });
			var listOfTerrains = new List<string> (splitTerrainList);

			listOfTerrains.Add (guid);

			editor.PutString ("terenac_terrain_list", String.Join(",", listOfTerrains));
			editor.Commit ();
		}
	}
}


