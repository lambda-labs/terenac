﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Terenac;

namespace Terenac
{
	
	public class LitologicalUnitsFragment : Fragment
	{
		class LitoInformation : Java.Lang.Object {
			public double quote;
			public int profile;
			public double interval;
			public double depth;
			public string material;

			public override string ToString ()
			{
				return quote + "::" + profile + "::" + interval + "::" + depth + "::" + material;
			}

			public static LitoInformation FromString(string s) {
				var split = s.Split (new string[] { "::" }, StringSplitOptions.None);
				LitoInformation li = new LitoInformation ();

				li.quote = double.Parse (split [0]);
				li.profile = int.Parse (split [1]);
				li.interval = double.Parse (split [2]);
				li.depth = double.Parse (split [3]);
				li.material = split [4];

				return li;
			}
		}								
			
		ObservableList<LitoInformation> currentLitoInformation = new ObservableList<LitoInformation>();

		private LinearLayout list = null;
		private FrameLayout newEntry = null;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var inflated = inflater.Inflate(Resource.Layout.LitoUnits, container, false);
			var header = inflated.FindViewById<FrameLayout> (Resource.Id.header);

			var head = inflater.Inflate (Resource.Layout.LitoHeader, null);
			header.AddView (head);

			list = inflated.FindViewById<LinearLayout>(Resource.Id.oldFields);

			// IMPORTANT!
			currentLitoInformation.ListChanged += UpdatingView;
				
			var newField = inflater.Inflate (Resource.Layout.LitoRow, null);

			var _quote = newField.FindViewById<TextView> (Resource.Id.quote);
			var _hatch = newField.FindViewById<ImageButton> (Resource.Id.hatch);
			var _interval = newField.FindViewById<EditText> (Resource.Id.interval);
			var _depth = newField.FindViewById<TextView> (Resource.Id.depth);
			var _material = newField.FindViewById<EditText> (Resource.Id.material);

			newEntry = inflated.FindViewById<FrameLayout> (Resource.Id.newField);
			newEntry.AddView (newField);

			var submit = inflated.FindViewById<Button> (Resource.Id.submit);
			submit.Click += (object sender, EventArgs e) => {
				double q; Double.TryParse(_quote.Text, out q);
				double d; Double.TryParse(_depth.Text, out d);
				double i; Double.TryParse(_interval.Text, out i);

				currentLitoInformation.Add (new LitoInformation () {
					quote = q,
					profile = 0,
					depth = d,
					interval = i,
					material = _material.Text
				});

				_interval.Text = "";
				_material.Text = "";

				currentLitoInformation.Notify();
			};

			RefreshManually ();

			return inflated;
		}

		private void UpdatingView() {
			var prefs = this.Activity.GetPrefs ();
			var inflater = this.Activity.LayoutInflater;

			string terrain = prefs.GetString ("terenac_current_working_terrain", null);

			if (terrain != null) {
				var editor = prefs.Edit ();

				editor.PutInt (terrain + "_dig_count", currentLitoInformation.Items.Count);

				list.RemoveAllViews();

				for (int i = 0; i < currentLitoInformation.Items.Count; i++) {
					var Element = currentLitoInformation.Items [i];
					var info = Element.ToString ();
					editor.PutString (terrain + "_dig_" + i, info);

					var newRow = inflater.Inflate (Resource.Layout.LitoRow, null);

					newRow.Tag = i;

					var quote = newRow.FindViewById<TextView> (Resource.Id.quote);
					var hatch = newRow.FindViewById<ImageButton> (Resource.Id.hatch);
					var interval = newRow.FindViewById<EditText> (Resource.Id.interval);
					var depth = newRow.FindViewById<TextView> (Resource.Id.depth);
					var material = newRow.FindViewById<EditText> (Resource.Id.material);

					quote.Text = "" + Element.quote;
					quote.Tag = i;
					interval.Text = "" + Element.interval;
					interval.Tag = i;
					depth.Text = "" + Element.depth;
					depth.Tag = i;
					material.Text = "" + Element.material;
					material.Tag = i;

					interval.FocusChange += (object sender, View.FocusChangeEventArgs e) => {
						Toast.MakeText(this.Activity, interval.Tag.ToString(), ToastLength.Short).Show();
						currentLitoInformation.Items[(int)interval.Tag].interval = double.Parse(interval.Text);
						currentLitoInformation.Notify();
					};

					material.FocusChange += (object sender, View.FocusChangeEventArgs e) => {
						currentLitoInformation.Items[(int)material.Tag].material = interval.Text;
						currentLitoInformation.Notify();
					};
						
					if (list != null) {
						list.AddView (newRow);
					}
				}

				editor.Commit ();
			}
		}

		public void RefreshManually() 
		{
			if (this.Activity == null)
				return;

			if (this.list == null)
				return;
			
			var prefs = this.Activity.GetPrefs ();

			string terrain = prefs.GetString ("terenac_current_working_terrain", null);

			list.RemoveAllViews ();
			currentLitoInformation.Items.Clear ();

			if (terrain != null) 
			{
				int digCount = prefs.GetInt (terrain + "_dig_count", 0);

				for (int i = 0; i < digCount; i++) {
					string digInfo = prefs.GetString (terrain + "_dig_" + i, null);
					var lito = LitoInformation.FromString (digInfo);
					currentLitoInformation.Add (lito);
				}

				double quoteNow = 0.0;
				double depthNow = 0.0;

				double.TryParse (prefs.GetString (terrain + "_z", "0"), out quoteNow);

				for (int i = 0; i < digCount; i++) {					
					var lito = currentLitoInformation.Items [i];
					lito.quote = quoteNow;
					lito.depth = depthNow;

					quoteNow -= lito.interval;
					depthNow += lito.interval;
				}

				currentLitoInformation.Notify ();
			}
		}
	}
}

